*** Settings ***
Documentation     A test case for super admin is able to view the users page
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.

Resource        ../../Resources/resources.robot

*** Test Cases ***
The Super Admin should be able to see Notification page.
    [TAGS]    super admin, sponsor, view

#    Open MBC Admin
    Open MBC Admin in New Page
    Sign in with user       ${SUPER_ADMIN_DETAILS}[email]    ${SUPER_ADMIN_DETAILS}[password]
    Display The notifications page
    Create Notification
    ${Get_title} =    Get Text    xpath=//*[@id="root"]/div/div[2]/div/div/div/div[2]/div[1]/div[1]     # Newly created notif
    Should Be Equal    ${notif}[title]     ${Get_title}
    Get Url    ==      ${URl}notif
    Click Delete in chosen notification
    Logout With User
    Close Browser